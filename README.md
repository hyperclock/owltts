# OwlTTS

is an opensource project to bring TTS (Text To Speech) to the desktop. Plan is to make this project usable on any Operating System capable of meeting the minimum requirements.

## Features

    * List coming soon.

## Documentation
None Yet.

## License
See [LICENSE](LICENSE) for details

## Contribute
Like everywhere else in the "GitWorld"

## Bugs, Issues, Suggests & more
You can do all that in our [Issues Area](../../issues)